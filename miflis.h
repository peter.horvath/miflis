#ifndef miflis_h
#define miflis_h

typedef float FLOAT;
typedef FLOAT VEC[3];
typedef FLOAT MTX[9];
typedef __uint32_t u32;
typedef __uint64_t u64;

#define EPSILON 0.000000001

// configuration of the whole simulation
#define WING_NO 3 // we have 3 wings
#define MASS_NO 6 // we approximate plane mechanics with 6 point-like masses
struct conf {
	FLOAT dt;
	VEC g;
	VEC pdrag;
	VEC pdrag_action_a;
	VEC v_init;
	VEC pos_init;
	VEC phi_init;
	MTX ori_init;
	VEC thrust_init;
	VEC thrust_action_a;
	struct {
		FLOAT idrag_c;
		FLOAT idrag_exp;
		FLOAT lift_c;
		FLOAT lift_exp;
		FLOAT angle_init;
		VEC action_a;
		VEC ori;
	} wing[WING_NO];
	struct {
		VEC pos_a;
		FLOAT m;
	} mass[MASS_NO];
};

// processed configuration data - calculated only once on initialization
struct procdconf {
	FLOAT m; // mass of the plane (kg)
	MTX theta; // moment of inertia
	MTX theta_inv; // inverse of the moment of inertia
	VEC center_of_mass; // weighted average of the mass points
	VEC pdrag_action;
	VEC thrust_action;
	VEC wing_action[WING_NO];
	VEC mass_pos[WING_NO];
};

// controls - these variables can be changed by the deck controller electronics
struct controls {
	VEC thrust; // thrust (N)
	FLOAT wing_angle[WING_NO]; // angle of the wings (rad)
};

// state of the plane - it describes the the actual simulation state
struct state {
	VEC pos; // actual position of the pane
	MTX ori; // orientation matrix of the plane
	VEC v; // speed (earth-referenced)
	VEC phi; // angular velocity of the plane (plane-referenced)
	FLOAT t;
	u64 tic;
};

// describes a force on the plane, in plane reference:
//   F: the force (action comes from conf)
//   a: caused acceleration (in plane-ref)
//   aa: caused angular acceleration (in plane-ref)
struct p_force {
	VEC F;
	VEC a;
	VEC aa;
};

// internal, calculated data of the plane - it will be cleared and recalculated in every tic
struct istate {
	MTX ori_inv; // inverse of the orientation matrix
	VEC p_a; // sum of plane-ref accelerations
	VEC p_aa; // sum of plane-ref angular accelerations
	VEC p_v; // velocity in plane-ref
	struct p_force p_pdrag; // force of the parasite drag
	struct p_force p_thrust; // force caused by the thrust
	struct {
		struct p_force p_idrag;
		struct p_force p_lift;
	} wing[WING_NO]; // forces caused by the wings
	VEC e_a; // acceleration in earth-ref
	VEC e_aa; // angular acceleration in earth-ref
};

// a full simulation scenario
struct scen {
	struct conf *conf;
	struct procdconf *procdconf;
	struct controls *controls;
	struct state *state;
	struct istate *istate;
};

// vector functions
void vec_zero(VEC dst);
void vec_cpy(VEC dst, const VEC src);
void vec_inc(VEC dst, const VEC src);
void vec_dec(VEC dst, const VEC src);
void vec_add(VEC dst, const VEC src1, const VEC src2);
void vec_minus(VEC dst, const VEC src1, const VEC src2);
void vec_fmul(VEC dst, const FLOAT c, const VEC src);

// indexed multiplication of vectors
void vec_imul(VEC dst, const VEC src1, const VEC src2);

// scalar multiplication of vectors
FLOAT vec_smul(const VEC v1, const VEC v2);

// vectorial multiplication
void vec_vmul(VEC dst, const VEC a, const VEC b);

// absolute square of a vector
FLOAT vec_absq(const VEC v);

// length of a vector
FLOAT vec_abs(const VEC v);

// normal of vector
void vec_norm(VEC dst, const VEC src);

// vectorial indexed square: v_i*abs(v_i)
void vec_iabsq(VEC dst, const VEC src);

// calculates v projected to axe
void vec_proj_vec(VEC dst, const VEC axe, const VEC v);

// calculates v projected to plane (given with normalvector)
void vec_proj_plane(VEC dst, const VEC plane, const VEC v);

// matrix functions
// 012
// 345
// 678
void mtx_zero(MTX dst);
void mtx_cpy(MTX dst, const MTX src);
void mtx_add(MTX dst, const MTX src1, const MTX src2);
void mtx_inc(MTX dst, const MTX src);
void mtx_mulf(MTX dst, const MTX src, const FLOAT f);
void mtx_mul_vec(VEC dst, const MTX m, const VEC vec);

// matrix transpose
void mtx_trans(MTX dst, const MTX src);

// determinant of a matrix
FLOAT mtx_det(const MTX src);

// adjugate of a matrix
void mtx_adj(MTX dst, const MTX src);

// inverse of a matrix
void mtx_inv(MTX dst, const MTX src);

// matrix of rotation around "angle" axis
void mtx_anglerot(MTX m, const VEC angle);

// matrix multiplication
// temporary variable is only used for the ability to handle if dst==src1 or dst==src2
//  but don't worry, the subexpression eliminator will optimize this out
void mtx_mul(MTX dst, const MTX src1, const MTX src2);

// theta matrix of a point-like mass on given coordinates
void theta_masspoint(MTX dst, const FLOAT m, const VEC r);

// calculates the acceleration and the angular acceleration.
// Input: m: mass of the body
//        theta_inv: inverse of the theta matrix of the body,
//        F: force vector
//        pac: point of action of the force
// Output: a: acceleration vector
//         aa: angular acceleration vector
void apply_force(VEC a, VEC aa, const FLOAT m, const MTX theta_inv, const VEC F, const VEC pac);

// ori[0..2]: x of plane in the reference of earth
// ori[3..5]: y of plane in the reference of earth
// ori[6..8]: z of plane in the reference of earth
// Thus: vec[0..2] in the ref of the plane is
//   vec[0]*ori[0..2]+vec[1]*ori[3..5]+vec[2]*ori[6..8] in the ref of the earth.
// 

// Transforms a vector from the reference of the plane to the reference of the earth
void vec_p2e(struct scen *scen, VEC dst, const MTX src);

// Transforms a vector from the reference of the earth to the reference of the plane
void vec_e2p(struct scen *scen, VEC dst, const VEC src);

// calculates an exponential wing force (drag or lift)
FLOAT wing_force(const FLOAT c_coeff, const FLOAT exp_coeff, const FLOAT alpha, const VEC v);

// applies a force in the plane reference and adds to the internal aa, a vectors
void istate_apply_force(struct p_force* p_force, struct scen *scen, const VEC F, const VEC act);

void state_cpy(struct state * const dst, const struct state * const src);
void config_process(struct scen * scen);
void controls_init(struct scen * scen);
void state_init(struct scen * scen);

// resets internal state, need to be called on every new tic
void istate_reset(struct istate * const istate);

void tic(struct scen *scen);

#endif
