#include <math.h>
#include <printf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

typedef float FLOAT;
typedef FLOAT VEC[3];
typedef FLOAT MTX[9];
typedef __uint32_t u32;

#define EPSILON 0.000000001

// configuration of the whole simulation
#define WING_NO 3 // we have 3 wings
#define MASS_NO 6 // we approximate plane mechanics with 6 point-like masses
static const struct {
	FLOAT dt;
	VEC g;
	VEC pdrag;
	VEC pdrag_action_a;
	VEC v_init;
	VEC pos_init;
	VEC phi_init;
	MTX ori_init;
	VEC thrust_init;
	VEC thrust_action_a;
	struct {
		FLOAT idrag_c;
		FLOAT idrag_exp;
		FLOAT lift_c;
		FLOAT lift_exp;
		FLOAT angle_init;
		VEC action_a;
		VEC ori;
	} wing[WING_NO];
	struct {
		VEC pos_a;
		FLOAT m;
	} mass[MASS_NO];
} conf = {
	dt: 0.1, // tic time (s)
	g: {0, 0, -9.81}, // gravity accel (m/s^2)
	pdrag: {0.01, 0.1, 1}, // accel force in the 3 directions in case of v=1 m/s
	pdrag_action_a: {-0.2, 0, 0}, // aerodinamical centre of the plane - action point of drag
	v_init: {100, 0, 0}, // initial velocity (m/s)
	pos_init: {0, 0, 100}, // initial position (m)
	phi_init: {0, 0, 0}, // initial angular velocity vector (rad/s)
	ori_init: {1, 0, 0, 0, 1, 0, 0, 0, 1}, // initial orientation (1)
	thrust_init: {50, 0, 0}, // initial thrust (N)
	thrust_action_a: {0.5, 0, 0}, // point of action of the thrust (m)
	wing: {
		{
		idrag_c: 0.1, // linear coefficient of the induced drag (N of drag, if v=1 m/s and alpha=1 rad) (N/rad/s^2)
		idrag_exp: 1.2, // exponencial coefficient of the induced drag (1)
		lift_c: 0.03, // linear coeff of lift
		lift_exp: 1.0, // exp coeff of lift
		angle_init: 0.1, // initial angle of the wing (rad)
		action_a: {0.4, 0.4, 0}, // point of action of the wing (m)
		ori: {0, 0, 1} // direction of lift (1)
		},{
		idrag_c: 0.1,
		idrag_exp: 1.2,
		lift_c: 0.03,
		lift_exp: 1.0,
		angle_init: 0.1,
		action_a: {0.4, -0.4, 0},
		ori: {0, 0, 1}
		},{
		idrag_c: 0.1,
		idrag_exp: 1.2,
		lift_c: 0.003,
		lift_exp: 1.0,
		angle_init: 0,
		action_a: {0, 0, 0},
		ori: {0, 1, 0}
		}
	},
	mass: {
		{{   0,   0,   0}, 2}, // body
		{{ 0.5,   0,   0}, 1}, // nose
		{{-0.2,   0,   0}, 1}, // tail
		{{ 0.4, 0.4,   0}, 1}, // left wing
		{{ 0.4,-0.4,   0}, 1}, // right wing
		{{-0.2,   0, 0.2}, 1} // fin
	}
};

// processed configuration data - calculated only once on initialization
struct {
	FLOAT m; // mass of the plane (kg)
	MTX theta; // moment of inertia
	MTX theta_inv; // inverse of the moment of inertia
	VEC center_of_mass; // weighted average of the mass points
	VEC pdrag_action;
	VEC thrust_action;
	VEC wing_action[WING_NO];
	VEC mass_pos[WING_NO];
} procdconf;

// controls - these variables can be changed by the deck controller electronics
struct {
	VEC thrust; // thrust (N)
	FLOAT wing_angle[WING_NO]; // angle of the wings (rad)
} controls;

// state of the plane - it describes the the actual simulation state
struct state {
	VEC pos; // actual position of the pane
	MTX ori; // orientation matrix of the plane
	VEC v; // speed (earth-referenced)
	VEC phi; // angular velocity of the plane (plane-referenced)
	FLOAT t;
	u32 tic;
};

// internal, calculated data of the plane - it will be cleared and recalculated in every tic
struct istate {
	MTX ori_inv; // inverse of the orientation matrix (for 
	VEC p_a; // sum of plane-ref accelerations
	VEC p_aa; // sum of plane-ref angular accelerations
	VEC p_v; // velocity in plane-ref
	VEC p_drag; // drag in planer-ref
	VEC e_a; // acceleration in earth-ref
	VEC e_aa; // angular acceleration in earth-ref
};

struct state state;
struct istate istate;

// vector functions
static inline void vec_zero(VEC dst) {
	dst[0]=0;
	dst[1]=0;
	dst[2]=0;
}

static inline void vec_cpy(VEC dst, const VEC src) {
	dst[0]=src[0];
	dst[1]=src[1];
	dst[2]=src[2];
}

static inline void vec_inc(VEC dst, const VEC src) {
	dst[0]+=src[0];
	dst[1]+=src[1];
	dst[2]+=src[2];
}

static inline void vec_dec(VEC dst, const VEC src) {
	dst[0]-=src[0];
	dst[1]-=src[1];
	dst[2]-=src[2];
}

static inline void vec_add(VEC dst, const VEC src1, const VEC src2) {
	dst[0]=src1[0]+src2[0];
	dst[1]=src1[1]+src2[1];
	dst[2]=src1[2]+src2[2];
}

static inline void vec_minus(VEC dst, const VEC src1, const VEC src2) {
	dst[0]=src1[0]-src2[0];
	dst[1]=src1[1]-src2[1];
	dst[2]=src1[2]-src2[2];
}

// multiply a vector with a float
static inline void vec_fmul(VEC dst, const FLOAT c, const VEC src) {
	dst[0]=src[0]*c;
	dst[1]=src[1]*c;
	dst[2]=src[2]*c;
}

// indexed multiplication of vectors
static inline void vec_imul(VEC dst, const VEC src1, const VEC src2) {
	dst[0]=src1[0]*src2[0];
	dst[1]=src1[1]*src2[1];
	dst[2]=src1[2]*src2[2];
}

// scalar multiplication of vectors
static inline FLOAT vec_smul(const VEC v1, const VEC v2) {
	return v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2];
}

// vectorial multiplication
static inline void vec_vmul(VEC dst, const VEC a, const VEC b) {
	dst[0]=a[1]*b[2]-a[2]*b[1];
	dst[1]=a[2]*b[0]-a[0]*b[2];
	dst[2]=a[0]*b[1]-a[1]*b[0];
}

static inline FLOAT vec_absq(const VEC v) {
	return vec_smul(v, v);
}

static inline FLOAT vec_abs(const VEC v) {
	return sqrt(vec_smul(v, v));
}

static inline void vec_norm(VEC dst, const VEC src) {
	vec_fmul(dst, 1.0/vec_abs(src), src);
}

// vectorial indexed square: v_i*abs(v_i)
static inline void vec_iabsq(VEC dst, const VEC src) {
	dst[0]=src[0]*abs(src[0]);
	dst[1]=src[1]*abs(src[1]);
	dst[2]=src[2]*abs(src[2]);
}

// calculates v projected to axe
static inline void vec_proj_vec(VEC dst, const VEC axe, const VEC v) {
	vec_fmul(dst, vec_smul(axe, v)/vec_absq(axe), axe);
}

// calculates v projected to plane (given with normalvector)
static inline void vec_proj_plane(VEC dst, const VEC plane, const VEC v) {
	VEC tmp;
	vec_proj_vec(tmp, plane, v);
	vec_minus(dst, v, tmp);
}

/*
static inline void vec_mulmtx(VEC dst, const MTX mtx, const VEC src) {
	VEC tmp;
	tmp[0]=mtx[0]*src[0]+mtx[1]*src[1]+mtx[2]*src[2];
	tmp[1]=mtx[3]*src[0]+mtx[4]*src[1]+mtx[5]*src[2];
	tmp[2]=mtx[6]*src[0]+mtx[7]*src[1]+mtx[8]*src[2];

	dst[0]=tmp[0];
	dst[1]=tmp[1];
	dst[2]=tmp[2];
}
*/

// matrix functions
static inline void mtx_zero(MTX dst) {
	for (int a=0;a<9;a++)
		dst[a]=0;
}

static inline void mtx_cpy(MTX dst, const MTX src) {
	for (int a=0;a<9;a++)
		dst[a]=src[a];
}

static inline void mtx_add(MTX dst, const MTX src1, const MTX src2) {
	for (int a=0;a<9;a++)
		dst[a]=src1[a]+src2[a];
}

static inline void mtx_inc(MTX dst, const MTX src) {
	mtx_add(dst, dst, src);
}

static inline void mtx_mulf(MTX dst, const MTX src, const FLOAT f) {
	for (int a=0;a<9;a++)
		dst[a]=src[a]*f;
}

static inline void mtx_mul_vec(VEC dst, const MTX m, const VEC vec) {
	VEC v={vec[0], vec[1], vec[2]};
	dst[0] = m[0]*v[0]+m[1]*v[1]+m[2]*v[2];
	dst[1] = m[3]*v[0]+m[4]*v[1]+m[5]*v[2];
	dst[2] = m[6]*v[0]+m[7]*v[1]+m[8]*v[2];
}

// 012
// 345
// 678

// matrix transpose
static inline void mtx_trans(MTX dst, const MTX src) {
	MTX tmp;

	// this two-staged solution is for handling mtx_trans(a,a) too
	// - no prob, common subexpression elimininator will optimize this

	tmp[0]=src[0]; tmp[1]=src[3]; tmp[2]=src[6];
	tmp[3]=src[1]; tmp[4]=src[4]; tmp[5]=src[7];
	tmp[6]=src[2]; tmp[7]=src[5]; tmp[8]=src[8];

	dst[0]=tmp[0]; dst[1]=tmp[1]; dst[2]=tmp[2];
	dst[3]=tmp[3]; dst[4]=tmp[4]; dst[5]=tmp[5];
	dst[6]=tmp[6]; dst[7]=tmp[7]; dst[8]=tmp[8];
}

// determinant of a matrix
static inline FLOAT mtx_det(MTX src) {
	FLOAT
		a=src[0], b=src[1], c=src[2],
		d=src[3], e=src[4], f=src[5],
		g=src[6], h=src[7], i=src[8];

	return a*e*i+b*f*g+c*d*h-g*e*c-h*f*a-i*d*b;
}

// adjugate of a matrix
static inline void mtx_adj(MTX dst, MTX src) {
	FLOAT
		a=src[0], b=src[1], c=src[2],
		d=src[3], e=src[4], f=src[5],
		g=src[6], h=src[7], i=src[8];

	dst[0]=e*i-f*h; dst[1]=c*h-b*i; dst[2]=b*f-c*e;
	dst[3]=f*g-d*i; dst[4]=a*i-c*g; dst[5]=c*d-a*f;
	dst[6]=d*h-e*g; dst[7]=b*g-a*h; dst[8]=a*e-b*d;
}

// inverse of a matrix
static inline void mtx_inv(MTX dst, MTX src) {
	mtx_adj(dst, src);
	mtx_mulf(dst, dst, 1.0/mtx_det(src));
}

// matrix of rotation around "angle" axis
static inline void mtx_anglerot(MTX m, const VEC angle) {
	FLOAT eta = vec_abs(angle);
	FLOAT si = sin(eta), co = cos(eta);

	if (eta<EPSILON) {
		FLOAT x = angle[0], y = angle[1], z = angle[2];
		FLOAT omcpesq = 0.5 - eta * eta / 24 + eta * eta * eta * eta / 720; // one minus cosinus per eta square
		FLOAT sepe = 1 - eta / 6 + eta * eta * eta / 120; // sinus eta per eta

		m[0] = co + x*x*omcpesq  ; m[1] = x*y*omcpesq-z*sepe; m[2] = x*z*omcpesq       ;
		m[3] = y*x*omcpesq+z*sepe; m[4] = co+y*y*omcpesq    ; m[5] = y*z*omcpesq-x*sepe;
		m[6] = z*x*omcpesq-y*sepe; m[7] = z*y*omcpesq+x*sepe; m[8] = co+z*z*omcpesq    ;
	} else {
		FLOAT x = angle[0]/eta, y = angle[1]/eta, z = angle[2]/eta;

		m[0] = co + x*x*(1-co); m[1] = x*y*(1-co)-z*si; m[2] = x*z*(1-co)+y*si;
		m[3] = y*x*(1-co)+z*si; m[4] = co + y*y*(1-co); m[5] = y*z*(1-co)-x*si;
		m[6] = z*x*(1-co)-y*si; m[7] = z*y*(1-co)+x*si; m[8] = co+z*z*(1-co)  ;
	}
	/*
	FLOAT eta = vec_abs(angle);
	FLOAT si = sin(eta), co = cos(eta);
	FLOAT x = angle[0]/eta, y = angle[1]/eta, z = angle[2]/eta;

	m[0] = co + x*x*(1-co); m[1] = x*y*(1-co)-z*si; m[2] = x*z*(1-co)+y*si;
	m[3] = y*x*(1-co)+z*si; m[4] = co + y*y*(1-co); m[5] = y*z*(1-co)-x*si;
	m[6] = z*x*(1-co)-y*si; m[7] = z*y*(1-co)+x*si; m[8] = co+z*z*(1-co)  ;
	*/
}

// temporary variable is only used for the ability to handle if dst==src1 or dst==src2
//  but don't worry, the subexpression eliminator will optimize this out
static inline void mtx_mul(MTX dst, const MTX src1, const MTX src2) {
	MTX result;
	mtx_zero(result);
	for (int a=0;a<3;a++)
		for (int b=0;b<3;b++)
			for (int c=0;c<3;c++)
				result[a*3+b]+=src1[a*3+c]*src2[c*3+b];
	mtx_cpy(dst, result);
}

// theta matrix of a point-like mass on given coordinates
static inline void theta_masspoint(MTX dst, FLOAT m, VEC r) {
	MTX res={
		r[1]*r[1]+r[2]*r[2], -r[0]*r[1]         , -r[0]*r[2]         ,
		-r[0]*r[1]         , r[0]*r[0]+r[2]*r[2], -r[1]*r[2]         ,
		-r[0]*r[2]         , -r[1]*r[2]         , r[0]*r[0]+r[1]*r[1]};
	mtx_mulf(dst, res, m);
}

// calculates the acceleration and the angular acceleration.
// Input: m: mass of the body
//        theta_inv: inverse of the theta matrix of the body,
//        F: force vector
//        pac: point of action of the force
// Output: a: acceleration vector
//         aa: angular acceleration vector
static inline void apply_force(VEC a, VEC aa, const FLOAT m, const MTX theta_inv, const VEC F, const VEC pac) {
	VEC torque;

	vec_fmul(a, 1.0/m, F);
	vec_vmul(torque, F, pac);
	mtx_mul_vec(aa, theta_inv, torque);
}

// ori[0..2]: x of plane in the reference of earth
// ori[3..5]: y of plane in the reference of earth
// ori[6..8]: z of plane in the reference of earth
// Thus: vec[0..2] in the ref of the plane is
//   vec[0]*ori[0..2]+vec[1]*ori[3..5]+vec[2]*ori[6..8] in the ref of the earth.
// 

// Transforms a vector from the reference of the plane to the reference of the earth
static inline void vec_p2e(VEC dst, const MTX src) {
	mtx_mul_vec(dst, state.ori, src);
}

// Transforms a vector from the reference of the earth to the reference of the plane
static inline void vec_e2p(VEC dst, const VEC src) {
	mtx_mul_vec(dst, istate.ori_inv, src);
}

// calculates an exponential wing force (drag or lift)
static inline FLOAT wing_force(const FLOAT c_coeff, const FLOAT exp_coeff, const FLOAT alpha, const VEC v) {
	return c_coeff * exp( alpha*exp_coeff ) * vec_absq(v);
}

// applies a force in the plane reference and adds to the internal aa, a vectors
static inline void istate_apply_force(const VEC F, const VEC act) {
	VEC a, aa;
	apply_force(a, aa, procdconf.m, procdconf.theta_inv, F, act);
	vec_inc(istate.p_a, a);
	vec_inc(istate.p_aa, aa);
}

static inline void state_cpy(struct state *dst, struct state *src) {
	memcpy(dst, src, sizeof(struct state));
}

// state dumper
#define dump_output_x 200
#define dump_output_y 10
#define dump_stack_x 200
#define dump_stack_y 50
#define dump_fmt_float "%11.5f"
#define dump_fmt_float_angle "%f"
#define dump_fmt_vec "%s %s %s"

static char dump_stack[dump_stack_y][dump_stack_x];
static int dump_stack_n;
static char dump_output[dump_output_y][dump_output_x];

void dump_init() {
	dump_stack_n=0;
	for (int a=0;a<dump_output_y;a++)
		dump_output[a][0]=0;
}

char *dump_float(FLOAT f) {
	char *r;
	snprintf(r=dump_stack[dump_stack_n++], dump_stack_x, dump_fmt_float, f);
	return r;
}

char *dump_vec(VEC v) {
	char *r;
	snprintf(r=dump_stack[dump_stack_n++], dump_stack_x, dump_fmt_vec, dump_float(v[0]), dump_float(v[1]), dump_float(v[2]));
	return r;
}

char *dump_angles(struct state *state) {
	char *l=&(dump_stack[dump_stack_n++][0]);
	l[0]=0;
	for (int a=0;a<WING_NO;a++)
		snprintf(l+strlen(l), dump_output_x-strlen(l)-1, a ? ", " dump_fmt_float_angle : dump_fmt_float_angle, controls.wing_angle[a]);
	return l;
}

char *dump_mtx(MTX m, int row) {
	VEC v={m[row*3], m[row*3+1], m[row*3+2]};
	return dump_vec(v);
}

void dump_state(struct state *state) {
	dump_init();

	snprintf (dump_output[0], dump_output_x-1, "tic: % 8i, time: %s, angles: %s",
		state->tic, dump_float(state->t), dump_angles(state));
	snprintf (dump_output[1], dump_output_x-1, "pos: %s, %s (ori)",
		dump_vec(state->pos), dump_mtx(state->ori, 0));
	snprintf (dump_output[2], dump_output_x-1, "v:   %s, %s",
		dump_vec(state->v), dump_mtx(state->ori, 1));
	snprintf (dump_output[3], dump_output_x-1, "phi: %s, %s",
		dump_vec(state->phi), dump_mtx(state->ori, 2));

	for (int a=0;dump_output[a][0];a++)
		printf ("%s\n", dump_output[a]);
	printf ("\n");
	fflush (stdout);
}

void test() {
};

void config_process() {
	// calculate mass of the plane
	procdconf.m=0;
	for (int a=0;a<MASS_NO;a++)
		procdconf.m+=conf.mass[a].m;

	// calculate mass center of the plane
	vec_zero(procdconf.center_of_mass);
	for (int a=0;a<MASS_NO;a++) {
		VEC weighted;
		vec_fmul(weighted, conf.mass[a].m, conf.mass[a].pos_a);
		vec_inc(procdconf.center_of_mass, weighted);
	}
	vec_fmul(procdconf.center_of_mass, 1.0/procdconf.m, procdconf.center_of_mass);

	// calculate mass points in plane-centered reference frame
	for (int a=0;a<MASS_NO;a++)
		vec_minus(procdconf.mass_pos[a], conf.mass[a].pos_a, procdconf.center_of_mass);
	
	// calculate angle attack points in plane-centered reference frame
	for (int a=0;a<WING_NO;a++)
		vec_minus(procdconf.wing_action[a], conf.wing[a].action_a, procdconf.center_of_mass);

	// calculate pdrag action in plane-centered reference
	vec_minus(procdconf.pdrag_action, conf.pdrag_action_a, procdconf.center_of_mass);

	// calculate thrust action in plane-centered reference
	vec_minus(procdconf.thrust_action, conf.thrust_action_a, procdconf.center_of_mass);

	// calculate theta of the plane
	mtx_zero(procdconf.theta);
	for (int a=0;a<MASS_NO;a++) {
		MTX theta_of_mass;
		theta_masspoint(theta_of_mass, conf.mass[a].m, procdconf.mass_pos[a]);
		mtx_inc(procdconf.theta, theta_of_mass);
	}

	// calculate inverse theta of the plane
	mtx_inv(procdconf.theta_inv, procdconf.theta);
}

void controls_init() {
	// initialize thrust
	vec_cpy(controls.thrust, conf.thrust_init);

	// initialize angle wings
	for (int a=0;a<WING_NO;a++)
		controls.wing_angle[a] = conf.wing[a].angle_init;
}

void state_init() {
	vec_cpy(state.pos, conf.pos_init);
	mtx_cpy(state.ori, conf.ori_init);
	vec_cpy(state.v, conf.v_init);
	vec_cpy(state.phi, conf.phi_init);
	state.tic = 0;
	state.t = 0;
}

// resets internal state, need to be called on every new tic
void istate_reset() {
	bzero(&istate, sizeof(struct istate));
}

static inline void tic() {
	struct state newstate;
	state_cpy(&newstate, &state);

	// calculate inverse of the orientation matrix
	mtx_inv(istate.ori_inv, state.ori);

	// calculate speed into plane reference
	vec_e2p(istate.p_v, state.v);

	/*
	// calculate parasite drag
	//   - speed(planeref) *(i) speed(planeref) *(i) dragvec(planeref)
	vec_imul(istate.p_drag, istate.p_v, istate.p_v);
	vec_imul(istate.p_drag, istate.p_drag, conf.pdrag);
	vec_fmul(istate.p_drag, -1, istate.p_drag);
	istate_apply_force(istate.p_drag, procdconf.pdrag_action);

	// adding thrust
	istate_apply_force(controls.thrust, procdconf.thrust_action);

	// calculate effects of the wings (induced drag + lift)
	for (int a=0;a<WING_NO;a++) {
		FLOAT idrag_abs;
		VEC idrag_norm;
		VEC F_idrag;

		VEC v_wingpar;
		FLOAT lift_abs;
		VEC lift_norm;
		VEC F_lift;
		
		// induced drag:
		//   length: with plane-ref v
		idrag_abs = wing_force(conf.wing[a].idrag_c, conf.wing[a].idrag_exp, abs(controls.wing_angle[a]), istate.p_v);
		//   direction: always into -1*state.v
		vec_norm(idrag_norm, istate.p_v);

		vec_fmul(F_idrag, -idrag_abs, idrag_norm);
		istate_apply_force(F_idrag, procdconf.wing_action[a]);

		// lift:
		//   length: with v projected into the plane of the wing
		vec_proj_plane(v_wingpar, conf.wing[a].ori, istate.p_v);
		lift_abs = wing_force(conf.wing[a].lift_c, conf.wing[a].lift_exp, controls.wing_angle[a], v_wingpar);

		//   direction: always parallel to the normal vector of the wings (for angle=0)
		vec_cpy(lift_norm, conf.wing[a].ori);

		vec_fmul(F_lift, lift_abs, lift_norm);
		istate_apply_force(F_lift, procdconf.wing_action[a]);
	}

	// transforming into earth reference
	vec_p2e(istate.e_a, istate.p_a);
	vec_p2e(istate.e_aa, istate.p_aa);
	*/

	// adding gravity
	vec_inc(istate.e_a, conf.g);

	// apply effect of acceleration to the velocity-like parameters (v & phi)
	VEC dv;
	vec_fmul(dv, conf.dt, istate.e_a);
	vec_add(newstate.v, state.v, dv);

	VEC dphi;
	vec_fmul(dphi, conf.dt, istate.e_aa);
	vec_add(newstate.phi, state.phi, dv);

	// apply v & phi to the position-like parameters (pos, ori)
	// it is so complex because we must calculate with the mean speed during a tic, or we got double acceleration
	VEC dpos_v, dpos_a;
	vec_fmul(dpos_v, conf.dt, state.v);
	vec_fmul(dpos_a, conf.dt*conf.dt, istate.e_a);
	vec_add(newstate.pos, state.pos, dpos_a);
	vec_inc(newstate.pos, dpos_v);

	// ori is rotated by state.phi*dt + 0.5*istate.p_aa*dt*dt
	VEC orirot_phi, orirot_aa, orirot;
	vec_fmul(orirot_phi, conf.dt, state.phi);
	vec_fmul(orirot_aa, 0.5*conf.dt*conf.dt, istate.p_aa);
	vec_add(orirot, orirot_phi, orirot_aa);
	
	// actual rotation of the orientation matrix
	MTX orirotmtx;
	mtx_anglerot(orirotmtx, orirot);
	mtx_mul(newstate.ori, state.ori, orirotmtx);

	// step timers
	newstate.tic++;
	newstate.t+=conf.dt;
	
	state_cpy(&state, &newstate);
}

int main(int argc, char **argv) {
	config_process();
	controls_init();
	state_init();

	//test();
	//dump_init();

	printf ("initial state:\n");
	dump_state(&state);

	// flight
	while (state.pos[2]>0) { // until crash :-)
		istate_reset();
		tic();
		dump_state(&state);
	}

	// crash
	printf("crash in tic %i (%fs), end state:\n", state.tic, state.t);
	dump_state(&state);
	return 0;
}
