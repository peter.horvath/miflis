CC:=gcc
CFLAGS:=-Wall -std=c99 -g

OBJS=miflis.o sim.o

GUI_JAR=gui/target/miflis-gui-1.0-SNAPSHOT-jar-with-dependencies.jar

default: sim

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

miflis.o: miflis.h
sim.o: miflis.h conf.h

help:
	@echo "Possible targets:"
	@echo ""
	@echo "help:     shows this help"
	@echo "miflis:   compiles miflis core"
	@echo "gui:      compiles miflis gui (calls maven in gui/)"
	@echo "startgui: starts gui"
	@echo "clean:    cleans miflis core"
	@echo "cleangui: cleans gui (calls maven in gui/)"

sim: $(OBJS)
	$(CC) -o $@ $(OBJS) -lm -g

gui: $(GUI_JAR)

$(GUI_JAR):
	cd gui && mvn clean package

startgui: $(GUI_JAR)
	java -jar $(GUI_JAR)

cleangui:
	cd gui && mvn clean

clean:
	rm -vf sim $(OBJS)

.PHONY: gui startgui
