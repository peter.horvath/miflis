#include <math.h>
#include <printf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "miflis.h"

// vector functions
void vec_zero(VEC dst) {
	dst[0]=0;
	dst[1]=0;
	dst[2]=0;
}

void vec_cpy(VEC dst, const VEC src) {
	dst[0]=src[0];
	dst[1]=src[1];
	dst[2]=src[2];
}

void vec_inc(VEC dst, const VEC src) {
	dst[0]+=src[0];
	dst[1]+=src[1];
	dst[2]+=src[2];
}

void vec_dec(VEC dst, const VEC src) {
	dst[0]-=src[0];
	dst[1]-=src[1];
	dst[2]-=src[2];
}

void vec_add(VEC dst, const VEC src1, const VEC src2) {
	dst[0]=src1[0]+src2[0];
	dst[1]=src1[1]+src2[1];
	dst[2]=src1[2]+src2[2];
}

void vec_minus(VEC dst, const VEC src1, const VEC src2) {
	dst[0]=src1[0]-src2[0];
	dst[1]=src1[1]-src2[1];
	dst[2]=src1[2]-src2[2];
}

// multiply a vector with a float
void vec_fmul(VEC dst, const FLOAT c, const VEC src) {
	dst[0]=src[0]*c;
	dst[1]=src[1]*c;
	dst[2]=src[2]*c;
}

// indexed multiplication of vectors
void vec_imul(VEC dst, const VEC src1, const VEC src2) {
	dst[0]=src1[0]*src2[0];
	dst[1]=src1[1]*src2[1];
	dst[2]=src1[2]*src2[2];
}

// scalar multiplication of vectors
FLOAT vec_smul(const VEC v1, const VEC v2) {
	return v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2];
}

// vectorial multiplication
void vec_vmul(VEC dst, const VEC a, const VEC b) {
	dst[0]=a[1]*b[2]-a[2]*b[1];
	dst[1]=a[2]*b[0]-a[0]*b[2];
	dst[2]=a[0]*b[1]-a[1]*b[0];
}

FLOAT vec_absq(const VEC v) {
	return vec_smul(v, v);
}

FLOAT vec_abs(const VEC v) {
	return sqrt(vec_smul(v, v));
}

void vec_norm(VEC dst, const VEC src) {
	vec_fmul(dst, 1.0/vec_abs(src), src);
}

// vectorial indexed square: v_i*abs(v_i)
void vec_iabsq(VEC dst, const VEC src) {
	dst[0]=src[0]*abs(src[0]);
	dst[1]=src[1]*abs(src[1]);
	dst[2]=src[2]*abs(src[2]);
}

// calculates v projected to axe
void vec_proj_vec(VEC dst, const VEC axe, const VEC v) {
	vec_fmul(dst, vec_smul(axe, v)/vec_absq(axe), axe);
}

// calculates v projected to plane (given with normalvector)
void vec_proj_plane(VEC dst, const VEC plane, const VEC v) {
	VEC tmp;
	vec_proj_vec(tmp, plane, v);
	vec_minus(dst, v, tmp);
}

// matrix functions
void mtx_zero(MTX dst) {
	for (int a=0;a<9;a++)
		dst[a]=0;
}

void mtx_cpy(MTX dst, const MTX src) {
	for (int a=0;a<9;a++)
		dst[a]=src[a];
}

void mtx_add(MTX dst, const MTX src1, const MTX src2) {
	for (int a=0;a<9;a++)
		dst[a]=src1[a]+src2[a];
}

void mtx_inc(MTX dst, const MTX src) {
	mtx_add(dst, dst, src);
}

void mtx_mulf(MTX dst, const MTX src, const FLOAT f) {
	for (int a=0;a<9;a++)
		dst[a]=src[a]*f;
}

void mtx_mul_vec(VEC dst, const MTX m, const VEC vec) {
	VEC v={vec[0], vec[1], vec[2]};
	dst[0] = m[0]*v[0]+m[1]*v[1]+m[2]*v[2];
	dst[1] = m[3]*v[0]+m[4]*v[1]+m[5]*v[2];
	dst[2] = m[6]*v[0]+m[7]*v[1]+m[8]*v[2];
}

// 012
// 345
// 678

// matrix transpose
void mtx_trans(MTX dst, const MTX src) {
	MTX tmp;

	// this two-staged solution is for handling mtx_trans(a,a) too
	// - don't worry, common subexpression elimininator will optimize this out

	tmp[0]=src[0]; tmp[1]=src[3]; tmp[2]=src[6];
	tmp[3]=src[1]; tmp[4]=src[4]; tmp[5]=src[7];
	tmp[6]=src[2]; tmp[7]=src[5]; tmp[8]=src[8];

	dst[0]=tmp[0]; dst[1]=tmp[1]; dst[2]=tmp[2];
	dst[3]=tmp[3]; dst[4]=tmp[4]; dst[5]=tmp[5];
	dst[6]=tmp[6]; dst[7]=tmp[7]; dst[8]=tmp[8];
}

// determinant of a matrix
FLOAT mtx_det(const MTX src) {
	FLOAT
		a=src[0], b=src[1], c=src[2],
		d=src[3], e=src[4], f=src[5],
		g=src[6], h=src[7], i=src[8];

	return a*e*i+b*f*g+c*d*h-g*e*c-h*f*a-i*d*b;
}

// adjugate of a matrix
void mtx_adj(MTX dst, const MTX src) {
	FLOAT
		a=src[0], b=src[1], c=src[2],
		d=src[3], e=src[4], f=src[5],
		g=src[6], h=src[7], i=src[8];

	dst[0]=e*i-f*h; dst[1]=c*h-b*i; dst[2]=b*f-c*e;
	dst[3]=f*g-d*i; dst[4]=a*i-c*g; dst[5]=c*d-a*f;
	dst[6]=d*h-e*g; dst[7]=b*g-a*h; dst[8]=a*e-b*d;
}

// inverse of a matrix
void mtx_inv(MTX dst, const MTX src) {
	mtx_adj(dst, src);
	mtx_mulf(dst, dst, 1.0/mtx_det(src));
}

// matrix of rotation around "angle" axis
void mtx_anglerot(MTX m, const VEC angle) {
	FLOAT eta = vec_abs(angle);
	FLOAT si = sin(eta), co = cos(eta);

	if (eta<EPSILON) {
		FLOAT x = angle[0], y = angle[1], z = angle[2];
		FLOAT omcpesq = 0.5 - eta * eta / 24 + eta * eta * eta * eta / 720; // one minus cosinus per eta square
		FLOAT sepe = 1 - eta / 6 + eta * eta * eta / 120; // sinus eta per eta

		m[0] = co + x*x*omcpesq  ; m[1] = x*y*omcpesq-z*sepe; m[2] = x*z*omcpesq       ;
		m[3] = y*x*omcpesq+z*sepe; m[4] = co+y*y*omcpesq    ; m[5] = y*z*omcpesq-x*sepe;
		m[6] = z*x*omcpesq-y*sepe; m[7] = z*y*omcpesq+x*sepe; m[8] = co+z*z*omcpesq    ;
	} else {
		FLOAT x = angle[0]/eta, y = angle[1]/eta, z = angle[2]/eta;

		m[0] = co + x*x*(1-co); m[1] = x*y*(1-co)-z*si; m[2] = x*z*(1-co)+y*si;
		m[3] = y*x*(1-co)+z*si; m[4] = co + y*y*(1-co); m[5] = y*z*(1-co)-x*si;
		m[6] = z*x*(1-co)-y*si; m[7] = z*y*(1-co)+x*si; m[8] = co+z*z*(1-co)  ;
	}
	/*
	FLOAT eta = vec_abs(angle);
	FLOAT si = sin(eta), co = cos(eta);
	FLOAT x = angle[0]/eta, y = angle[1]/eta, z = angle[2]/eta;

	m[0] = co + x*x*(1-co); m[1] = x*y*(1-co)-z*si; m[2] = x*z*(1-co)+y*si;
	m[3] = y*x*(1-co)+z*si; m[4] = co + y*y*(1-co); m[5] = y*z*(1-co)-x*si;
	m[6] = z*x*(1-co)-y*si; m[7] = z*y*(1-co)+x*si; m[8] = co+z*z*(1-co)  ;
	*/
}

// temporary variable is only used for the ability to handle if dst==src1 or dst==src2
//  but don't worry, the subexpression eliminator will optimize this out
void mtx_mul(MTX dst, const MTX src1, const MTX src2) {
	MTX result;
	mtx_zero(result);
	for (int a=0;a<3;a++)
		for (int b=0;b<3;b++)
			for (int c=0;c<3;c++)
				result[a*3+b]+=src1[a*3+c]*src2[c*3+b];
	mtx_cpy(dst, result);
}

// theta matrix of a point-like mass on given coordinates
void theta_masspoint(MTX dst, const FLOAT m, const VEC r) {
	MTX res={
		r[1]*r[1]+r[2]*r[2], -r[0]*r[1]         , -r[0]*r[2]         ,
		-r[0]*r[1]         , r[0]*r[0]+r[2]*r[2], -r[1]*r[2]         ,
		-r[0]*r[2]         , -r[1]*r[2]         , r[0]*r[0]+r[1]*r[1]};
	mtx_mulf(dst, res, m);
}

// calculates the acceleration and the angular acceleration.
// Input: m: mass of the body
//        theta_inv: inverse of the theta matrix of the body,
//        F: force vector
//        pac: point of action of the force
// Output: a: acceleration vector
//         aa: angular acceleration vector
void apply_force(VEC a, VEC aa, const FLOAT m, const MTX theta_inv, const VEC F, const VEC pac) {
	VEC torque;

	vec_fmul(a, 1.0/m, F);
	vec_vmul(torque, F, pac);
	mtx_mul_vec(aa, theta_inv, torque);
}

// ori[0..2]: x of plane in the reference of earth
// ori[3..5]: y of plane in the reference of earth
// ori[6..8]: z of plane in the reference of earth
// Thus: vec[0..2] in the ref of the plane is
//   vec[0]*ori[0..2]+vec[1]*ori[3..5]+vec[2]*ori[6..8] in the ref of the earth.
// 

// Transforms a vector from the reference of the plane to the reference of the earth
void vec_p2e(struct scen *scen, VEC dst, const MTX src) {
	mtx_mul_vec(dst, scen->state->ori, src);
}

// Transforms a vector from the reference of the earth to the reference of the plane
void vec_e2p(struct scen *scen, VEC dst, const VEC src) {
	mtx_mul_vec(dst, scen->istate->ori_inv, src);
}

// calculates an exponential wing force (drag or lift)
FLOAT wing_force(const FLOAT c_coeff, const FLOAT exp_coeff, const FLOAT alpha, const VEC v) {
	return c_coeff * exp( alpha*exp_coeff ) * vec_absq(v);
}

// applies a force in the plane reference and adds to the internal aa, a vectors
void istate_apply_force(struct p_force* p_force, struct scen *scen, const VEC F, const VEC act) {
	vec_cpy(p_force->F, F);
	apply_force(p_force->a, p_force->aa, scen->procdconf->m, scen->procdconf->theta_inv, F, act);
	vec_inc(scen->istate->p_a, p_force->a);
	vec_inc(scen->istate->p_aa, p_force->aa);
}

void state_cpy(struct state * const dst, const struct state * const src) {
	memcpy(dst, src, sizeof(struct state));
}

void config_process(struct scen *scen) {
	// calculate mass of the plane
	scen->procdconf->m=0;
	for (int a=0;a<MASS_NO;a++)
		scen->procdconf->m+=scen->conf->mass[a].m;

	// calculate mass center of the plane
	vec_zero(scen->procdconf->center_of_mass);
	for (int a=0;a<MASS_NO;a++) {
		VEC weighted;
		vec_fmul(weighted, scen->conf->mass[a].m, scen->conf->mass[a].pos_a);
		vec_inc(scen->procdconf->center_of_mass, weighted);
	}
	vec_fmul(scen->procdconf->center_of_mass, 1.0/scen->procdconf->m, scen->procdconf->center_of_mass);

	// calculate mass points in plane-centered reference frame
	for (int a=0;a<MASS_NO;a++)
		vec_minus(scen->procdconf->mass_pos[a], scen->conf->mass[a].pos_a, scen->procdconf->center_of_mass);
	
	// calculate angle attack points in plane-centered reference frame
	for (int a=0;a<WING_NO;a++)
		vec_minus(scen->procdconf->wing_action[a], scen->conf->wing[a].action_a, scen->procdconf->center_of_mass);

	// calculate pdrag action in plane-centered reference
	vec_minus(scen->procdconf->pdrag_action, scen->conf->pdrag_action_a, scen->procdconf->center_of_mass);

	// calculate thrust action in plane-centered reference
	vec_minus(scen->procdconf->thrust_action, scen->conf->thrust_action_a, scen->procdconf->center_of_mass);

	// calculate theta of the plane
	mtx_zero(scen->procdconf->theta);
	for (int a=0;a<MASS_NO;a++) {
		MTX theta_of_mass;
		theta_masspoint(theta_of_mass, scen->conf->mass[a].m, scen->procdconf->mass_pos[a]);
		mtx_inc(scen->procdconf->theta, theta_of_mass);
	}

	// calculate inverse theta of the plane
	mtx_inv(scen->procdconf->theta_inv, scen->procdconf->theta);
}

void controls_init(struct scen *scen) {
	// initialize thrust
	vec_cpy(scen->controls->thrust, scen->conf->thrust_init);

	// initialize angle wings
	for (int a=0;a<WING_NO;a++)
		scen->controls->wing_angle[a] = scen->conf->wing[a].angle_init;
}

void state_init(struct scen *scen) {
	vec_cpy(scen->state->pos, scen->conf->pos_init);
	mtx_cpy(scen->state->ori, scen->conf->ori_init);
	vec_cpy(scen->state->v, scen->conf->v_init);
	vec_cpy(scen->state->phi, scen->conf->phi_init);
	scen->state->tic = 0;
	scen->state->t = 0;
}

// resets internal state, need to be called on every new tic
void istate_reset(struct istate *const istate) {
	bzero(istate, sizeof(struct istate));
}

void tic(struct scen *scen) {
	struct state newstate;
	state_cpy(&newstate, scen->state);

	// calculate inverse of the orientation matrix
	mtx_inv(scen->istate->ori_inv, scen->state->ori);

	// calculate speed into plane reference
	vec_e2p(scen, scen->istate->p_v, scen->state->v);

	// adding thrust
	istate_apply_force(&(scen->istate->p_thrust), scen, scen->controls->thrust, scen->procdconf->thrust_action);

	// calculate parasite drag
	//   - speed(planeref) *(i) speed(planeref) *(i) dragvec(planeref)
	VEC p_pdrag;
	vec_imul(p_pdrag, scen->istate->p_v, scen->istate->p_v);
	vec_imul(p_pdrag, p_pdrag, scen->conf->pdrag);
	vec_fmul(p_pdrag, -1, p_pdrag);
	istate_apply_force(&(scen->istate->p_pdrag), scen, p_pdrag, scen->procdconf->pdrag_action);

	// calculate effects of the wings (induced drag + lift)
	for (int a=0;a<WING_NO;a++) {
		FLOAT idrag_abs;
		VEC idrag_norm;
		VEC F_idrag;

		VEC v_wingpar;
		FLOAT lift_abs;
		VEC lift_norm;
		VEC F_lift;
		
		// induced drag:
		//   length: with plane-ref v
		idrag_abs = wing_force(scen->conf->wing[a].idrag_c, scen->conf->wing[a].idrag_exp,
			abs(scen->controls->wing_angle[a]), scen->istate->p_v);

		//   direction: always into -1*state.v
		vec_norm(idrag_norm, scen->istate->p_v);

		vec_fmul(F_idrag, -idrag_abs, idrag_norm);
		istate_apply_force(&(scen->istate->wing[a].p_idrag), scen, F_idrag, scen->procdconf->wing_action[a]);

		// lift:
		//   length: with v projected into the plane of the wing
		vec_proj_plane(v_wingpar, scen->conf->wing[a].ori, scen->istate->p_v);
		lift_abs = wing_force(scen->conf->wing[a].lift_c, scen->conf->wing[a].lift_exp,
			scen->controls->wing_angle[a], v_wingpar);

		//   direction: always parallel to the normal vector of the wings (for angle=0)
		vec_cpy(lift_norm, scen->conf->wing[a].ori);

		vec_fmul(F_lift, lift_abs, lift_norm);
		istate_apply_force(&(scen->istate->wing[a].p_lift), scen, F_lift, scen->procdconf->wing_action[a]);
	}

	// transforming into earth reference
	vec_p2e(scen, scen->istate->e_a, scen->istate->p_a);
	vec_p2e(scen, scen->istate->e_aa, scen->istate->p_aa);

	// adding gravity
	//vec_inc(scen->istate->e_a, scen->conf->g);

	// apply effect of acceleration to the velocity-like parameters (v & phi)
	VEC dv;
	vec_fmul(dv, scen->conf->dt, scen->istate->e_a);
	vec_add(newstate.v, scen->state->v, dv);

	VEC dphi;
	vec_fmul(dphi, scen->conf->dt, scen->istate->e_aa);
	vec_add(newstate.phi, scen->state->phi, dphi);

	// apply v & phi to the position-like parameters (pos, ori)
	// it is so complex because we must calculate with the mean speed during a tic, or we got double acceleration
	VEC dpos_v, dpos_a;
	vec_fmul(dpos_v, scen->conf->dt, scen->state->v);
	vec_fmul(dpos_a, 0.5*scen->conf->dt*scen->conf->dt, scen->istate->e_a);
	vec_add(newstate.pos, scen->state->pos, dpos_a);
	vec_inc(newstate.pos, dpos_v);

	// ori is rotated by state.phi*dt + 0.5*istate.p_aa*dt*dt
	VEC orirot_phi, orirot_aa, orirot;
	vec_fmul(orirot_phi, scen->conf->dt, scen->state->phi);
	vec_fmul(orirot_aa, 0.5*scen->conf->dt*scen->conf->dt, scen->istate->p_aa);
	vec_add(orirot, orirot_phi, orirot_aa);
	
	// actual rotation of the orientation matrix
	MTX orirotmtx;
	mtx_anglerot(orirotmtx, orirot);
	mtx_mul(newstate.ori, scen->state->ori, orirotmtx);

	// step timers
	newstate.tic++;
	newstate.t=scen->conf->dt*newstate.tic;
	
	state_cpy(scen->state, &newstate);
}
