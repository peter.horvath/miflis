package maxx;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;

/**
 * Main app class
 */
public class App {
    public static void main( String[] args ) {
		Display display = new Display();
		Shell shell = new Shell(display);

		Button button = new Button(shell, SWT.PUSH);

		shell.setLayout(new FillLayout());

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();

        //System.out.println( "Hello World!" );
    }
}
