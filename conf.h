#ifndef miflis_conf_h
#define miflis_conf_h

static const struct conf conf = {
	dt: 0.1, // tic time (s)
	g: {0, 0, -9.81}, // gravity accel (m/s^2)
	pdrag: {0.01, 0.1, 1}, // accel force in the 3 directions in case of v=1 m/s
	pdrag_action_a: {-0.2, 0, 0}, // aerodinamical centre of the plane - action point of drag
	v_init: {100, 0, 0}, // initial velocity (m/s)
	pos_init: {0, 0, 100}, // initial position (m)
	phi_init: {0, 0, 0}, // initial angular velocity vector (rad/s)
	ori_init: {1, 0, 0, 0, 1, 0, 0, 0, 1}, // initial orientation (1)
	thrust_init: {50, 0, 0}, // initial thrust (N)
	thrust_action_a: {0.5, 0, 0}, // point of action of the thrust (m)
	wing: {
		{
		idrag_c: 0.1, // linear coefficient of the induced drag (N of drag, if v=1 m/s and alpha=1 rad) (N/rad/s^2)
		idrag_exp: 1.2, // exponencial coefficient of the induced drag (1)
		lift_c: 0.03, // linear coeff of lift
		lift_exp: 1.0, // exp coeff of lift
		angle_init: 0.1, // initial angle of the wing (rad)
		action_a: {0.4, 0.4, 0}, // point of action of the wing (m)
		ori: {0, 0, 1} // direction of lift (1)
		},{
		idrag_c: 0.1,
		idrag_exp: 1.2,
		lift_c: 0.03,
		lift_exp: 1.0,
		angle_init: 0.1,
		action_a: {0.4, -0.4, 0},
		ori: {0, 0, 1}
		},{
		idrag_c: 0.1,
		idrag_exp: 1.2,
		lift_c: 0.003,
		lift_exp: 1.0,
		angle_init: 0,
		action_a: {0, 0, 0},
		ori: {0, 1, 0}
		}
	},
	mass: {
		{{   0,   0,   0}, 2}, // body
		{{ 0.5,   0,   0}, 1}, // nose
		{{-0.2,   0,   0}, 1}, // tail
		{{ 0.4, 0.4,   0}, 1}, // left wing
		{{ 0.4,-0.4,   0}, 1}, // right wing
		{{-0.2,   0, 0.2}, 1} // fin
	}
};

#endif
