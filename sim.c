#include <math.h>
#include <printf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "miflis.h"
#include "conf.h"

struct procdconf procdconf;
struct controls controls;
struct state state;
struct istate istate;

struct scen scen = {
	conf: (struct conf*)&conf,
	procdconf: &procdconf,
	controls: &controls,
	state: &state,
	istate: &istate,
};

// state dumper
#define dump_output_x 200
#define dump_output_y 10
#define dump_stack_x 200
#define dump_stack_y 50
#define dump_fmt_float "%11.5f"
#define dump_fmt_float_angle "%f"
#define dump_fmt_vec "%s %s %s"

static char dump_stack[dump_stack_y][dump_stack_x];
static int dump_stack_n;
static char dump_output[dump_output_y][dump_output_x];

void dump_init() {
	dump_stack_n=0;
	for (int a=0;a<dump_output_y;a++)
		dump_output[a][0]=0;
}

char *dump_float(FLOAT f) {
	char *r;
	snprintf(r=dump_stack[dump_stack_n++], dump_stack_x, dump_fmt_float, f);
	return r;
}

char *dump_vec(VEC v) {
	char *r;
	snprintf(r=dump_stack[dump_stack_n++], dump_stack_x, dump_fmt_vec, dump_float(v[0]), dump_float(v[1]), dump_float(v[2]));
	return r;
}

char *dump_angles(struct state *state) {
	char *l=&(dump_stack[dump_stack_n++][0]);
	l[0]=0;
	for (int a=0;a<WING_NO;a++)
		snprintf(l+strlen(l), dump_output_x-strlen(l)-1, a ? ", " dump_fmt_float_angle : dump_fmt_float_angle, controls.wing_angle[a]);
	return l;
}

char *dump_mtx(MTX m, int row) {
	VEC v={m[row*3], m[row*3+1], m[row*3+2]};
	return dump_vec(v);
}

void dump_scen(struct scen *scen) {
	dump_init();

	snprintf (dump_output[0], dump_output_x-1, "tic: % 20lli, time: %s, angles: %s",
		(long long int)(scen->state->tic), dump_float(scen->state->t), dump_angles(scen->state));
	snprintf (dump_output[1], dump_output_x-1, "pos: %s, %s (ori)",
		dump_vec(scen->state->pos), dump_mtx(scen->state->ori, 0));
	snprintf (dump_output[2], dump_output_x-1, "v:   %s, %s",
		dump_vec(scen->state->v), dump_mtx(scen->state->ori, 1));
	snprintf (dump_output[3], dump_output_x-1, "phi: %s, %s",
		dump_vec(scen->state->phi), dump_mtx(scen->state->ori, 2));

	for (int a=0;dump_output[a][0];a++)
		printf ("%s\n", dump_output[a]);
	printf ("\n");
	fflush (stdout);
}

int main(int argc, char **argv) {
	config_process(&scen);
	controls_init(&scen);
	state_init(&scen);

	//test();
	//dump_init();

	printf ("sizes: istate: %lu\n", sizeof(struct istate));

	printf ("initial state:\n");
	dump_scen(&scen);

	// flight
	while (scen.state->pos[2]>0) { // until crash :-)
		istate_reset(scen.istate);
		tic(&scen);
		dump_scen(&scen);
	}

	// crash
	printf("crash in tic %llu (%fs), end state:\n", (long long unsigned int)state.tic, state.t);
	dump_scen(&scen);
	return 0;
}
